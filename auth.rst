Authentication
=======================================================

Login and Register
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Call this method to show the authentication webview

.. code-block:: c#
    
        GTVSDK.Instance.ShowLogin(System.Action<string, string> loginCallback, System.Action<string, string> playnowCallback, System.Action<string, string> fbLoginCallback)
        // loginCallback: a callback that return api_token and uuid if login with ID success (the first param is api_token, the second is uuid)
        // playnowCallback: a callback that return api_token and uuid if success (the first param is api_token, the second is uuid)
        // fbLoginCallback: a callback that return api_token and uuid if success (the first param is api_token, the second is uuid)
