Payment
=======================================================

IAP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Call this method to start the payment process

.. code-block:: c#
    
        BuyProduct(string productId, GameUserData userData, System.Action<IAPResponse> onPurchaseCompleted)
        // productId: Id of the product
        // userData: an object contain user information
        // onIapCompleted: a callback return the status and data of IAP flow

.. code-block:: c#

        // the content of this class is depended on the partner
        public class GameUserData
        {
            public int level;            
            public int roleId;
            public int serverId;            
            public string product_id;
        }

.. code-block:: c#
        
        public class IAPResponse
        {
            public IAPResponseCode ErrorCode { get; set; } // ErrorCode = 1: success, 11: cancel, other: failed
            public string Message { get; set; }
            public string Receipt { get; set; }
        }