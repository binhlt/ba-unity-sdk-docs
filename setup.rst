Prerequisite
=======================================================

Dependency
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The SDK include several library. They are:

- Facebook SDK
- Firebase SDK
- JsonDotnet
- Parse
- Unity-webview (https://github.com/gree/unity-webview)

Required Information
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Some information we must provided for the partner to use. Please ask for it if you don't see it. They are:

- Facebook App Id
- Firebase google-services.json file
- Firebase GoogleService-Info.plist file
- SDK workflow and server api documentation
- Android keystore and password for it
- Apple provision profile (Adhoc and Production)
- Apple certificate and password for it

Unity Services
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
These Unity Services may need to be enabled to allow the SDK to function

- Unity Purchasing

iOS configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- Add IAP capability
- Add Push Notification capability
- Add Background mode capability and enable remote notification
- Add Apple Signin capability
